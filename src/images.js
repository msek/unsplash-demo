export const imagesList = [
    {
        src: 'https://source.unsplash.com/random?1',
        title: 'Some random image'
    },
    {
        src: 'https://source.unsplash.com/random?2',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?3',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?4',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?5',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?6',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?7',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?8',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?9',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?10',
        title: 'Yet another random image'
    },
    {
        src: 'https://source.unsplash.com/random?11',
        title: 'Yet another random image'
    }
]