import React, { useState } from 'react';
import './App.css';
import {imagesList} from './images';

function App() {
  const [images, setImages] = useState(imagesList);
  const [activeImage, setActiveImage] = useState(false);

  const updateImageTitle = (index, e) => 
    setImages(
      images.map(
        (image, i) =>
          index === i ? { ...image, title: e.target.value } : image
      )
    );

  const deleteImage = (index) => {
    setActiveImage(false);
    setImages(
      images.filter(
        (image, i) => 
          i !== index
      )
    );
  }

  const renderImages = (images) =>
    images.map((img, index) =>
      <div className='gallery-item' key={index}>
        <p>{img.title}</p>
        <img src={img.src} alt={img.title} onClick={() => setActiveImage(index)} />
      </div>
    );

  const renderActiveImage = (index) =>
    [
      <img
        src={images[index].src}
        alt={images[index].title}
        key='activeImg'
      />,
      <div className='image-controls'>

        <button className='button-delete' onClick={() => deleteImage(index)}>
          Delete image
        </button>
        <input
          type='text'
          className='input-text'
          value={images[index].title}
          onChange={(e) => updateImageTitle(index, e)}
          key='activeImgTitle'
        />
      </div>,
      <span className='modal-close' onClick={() => setActiveImage(false)}>
        X
      </span>
    ]

  return (
    <div className='app'>
      <header className='header'>
        <h1>Unsplash Gallery</h1>
      </header>
      <main className='gallery'>
        { images ? renderImages(images) : 'No images here. Sorry' }
        <div className={`image-modal ${activeImage === false && 'hidden'}`}>
          { activeImage !== false && renderActiveImage(activeImage) }
        </div>
      </main>
    </div>
  );
}

export default App;
